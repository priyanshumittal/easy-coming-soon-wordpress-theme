<?php 
/*
 * Template Name: Features Details Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

<!-- ============================================================= SECTION – ACCORDION ============================================================= -->
			
<section id="standard-tabs-side" class="border-top">
				<div class="container inner">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Frequently Asked Questions</h1>
								<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor.</p>
							</header>
						</div><!-- /.col -->
					</div><!-- ./row -->
					
					<div class="row">
						<div class="col-xs-12 inner-top inner-bottom-sm">
							<div class="tabs tabs-side tab-container">
								
								<ul class="etabs">
									<li class="tab"><a href="#tab-1">General Settings</a></li>
									<li class="tab"><a href="#tab-2">Page Settings</a></li>
									<li class="tab"><a href="#tab-3">Header Settings</a></li>
									<li class="tab"><a href="#tab-4">Countdown Settings</a></li>
									<li class="tab"><a href="#tab-5">Progress Bar Settings</a></li>
									<li class="tab"><a href="#tab-6">Footer Settings</a></li>
									<li class="tab"><a href="#tab-7">Language Settings</a></li>
									<li class="tab"><a href="#tab-8">Templates</a></li>
									<li class="tab"><a href="#tab-9">Background Settings</a></li>
									<li class="tab"><a href="#tab-10">Text & Color</a></li>
									<li class="tab"><a href="#tab-11">Container Settings</a></li>
									<li class="tab"><a href="#tab-12">Custom Codes</a></li>
									<li class="tab"><a href="#tab-13">Social Settings</a></li>
									<li class="tab"><a href="#tab-14">Notification Settings</a></li>
									<li class="tab"><a href="#tab-15">Form Field Descriptions</a></li>
									<li class="tab"><a href="#tab-16">Access Settings</a></li>
									
								</ul><!-- /.etabs -->
								
								<div class="panel-container">
									
									<div class="tab-content" id="tab-1">
										<div class="row">
											
											<div class="col-md-12 inner-left-xs">
												<h3>1. About Easy Coming Soon?</h3>
												<p>Easy Coming Soon Plugin for display coming soon landing page and website maintenance. Before launching your website and want to show  impressive and elegant landing page for your visitor. There are four types of templates variation and you can show your development progress for your visitor, using progress bar feature. Customize your landing page by option panel. Here are many features in this Plugin like that change your brand name, background image, countdown timer setting, progress bar setting, contact info, template design, social profiles etc.Easy Coming Soon Plugin for display coming soon landing page and website maintenance. Before launching your website and want to show  impressive and elegant landing page for your visitor. There are four types of templates variation and you can show your development progress for your visitor, using progress bar feature. Customize your landing page by option panel. Here are many features in this Plugin like that change your brand name, background image, countdown timer setting, progress bar setting, contact info, template design, social profiles etc. </p>
												
												<p><iframe width="560" height="315" src="https://www.youtube.com/embed/JEbKUdvbzys" frameborder="0" allowfullscreen></iframe></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-top-xs inner-left-xs">
												<h3>2. How to Enable the Maintenance Mode?</h3>
												<p style="text-align: justify">You will need enable to this setting for display coming soon template. You can see multiple options here for disable and enable coming soon mode, enable maintenance mode. Better clarifications see below screenshot of general setting and follow steps. Are you confused between Maintenance Mode and Coming Soon Mode ? <a title="Difference Between Maintenance Mode and Coming Soon Mode?" href="http://webriti.com/help/ecs/what-is-the-difference-between-maintenance-mode-and-coming-soon-mode/" target="_blank">Click Here</a></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-top-xs inner-left-xs">
												<h3>3. Difference Between Maintenance Mode and Coming Soon Mode?</h3>
												<p>The main difference is that the Maintenance Mode returns the Status code 503. This status code basically tells the search engines that the site temporarily down and will be up soon. </p>
											</div><!-- /.col -->
											
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-2">
										<div class="row">
											
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to Upload My Logo, Headline & Description?</h3>
												<p>In Page setting you can change logo, headline & description. You can follow below steps and image.</p>
												
												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt; Page Setting</strong></p>
												
												<ul>
													<li><strong>Upload Your Logo -&gt;</strong> You can upload logo using <strong>Upload</strong> button and can see preview your selected logo.</li>
													<li><strong>Headline -&gt;</strong> Enter your headline in <strong>Headline</strong> input field.</li>
													<li><strong>Description -&gt;</strong> Enter your description in <strong>Description</strong> input field.</li>
												</ul>
												
												<p>After done your changes. Please don’t forget click on “Save Changes” button. Now your filled details will appear on homepage.</p>
												
												<a href="http://webriti.com/help/wp-content/uploads/2015/04/new-2.jpg"><img class="alignnone size-full wp-image-833" src="http://webriti.com/help/wp-content/uploads/2015/04/new-2.jpg" alt="new-2" width="1390" height="757"></a>
											</div><!-- /.col -->
											
								
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-3">
										<div class="row">
											
											<div class="col-md-12 inner-left-xs">
												<h3>1. Header Customization?</h3>
												<p>In header setting you can change Favicon, SEO Meta Title, SEO Meta Description and Google Analytics Code. This is SEO feature. You no need any SEO Plugin. This plugin is SEO ready.</p>
												
												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt; Header Setting</strong></p>
												
												<ul>
													<li><strong>Favicon</strong> -&gt; Click on <strong>Upload</strong> button and change your favicon. If you want to see preview of favicon ? click on Preview button and you can remove favicon using Remove Favicon button.</li>
													<li><strong>SEO Meta Title -&gt;</strong> Set your own title like your business name etc.</li>
													<li><strong>SEO Meta Description -&gt;</strong> Enter your website description.</li>
													<li><strong>Google Analytics Code -&gt;</strong> Enter your Google Analytics Code.</li>
												</ul>
												
												<p>After done your changes. Please don’t forget click on “Save Changes” button. Now your filled details will appear on homepage.</p>
												
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-3.jpg"><img class="alignnone size-full wp-image-835" src="http://webriti.com/help/wp-content/uploads/2015/04/new-3.jpg" alt="new-3" width="1386" height="945"></a></p>
												
											</div><!-- /.col -->
											
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-4">
										<div class="row">
										
											<div class="col-md-12 inner-left-xs">
												<h3>1. Count Down Timer Settings</h3>
												
												<p>If You want to display countdown timer on homepage? You need to <strong>Enable Countdown</strong> using on ON/OFF button. After click on button you can see more setting like that below.</p>
												
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/11.png"><img class="alignnone size-full wp-image-172" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/11.png" alt="11" width="1466" height="476"></a></p>
												
												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt; Countdown Setting</strong></p>
												
												<ul>
													<li><strong>Countdown Styles -&gt; </strong>If you want change style of Countdown timer. <strong><a title="Style1" href="http://webriti.com/help/wp-content/uploads/2015/04/style1.png" target="_blank">Style1</a></strong> means that your timer box will show in square and <strong><a title="Style2" href="http://webriti.com/help/wp-content/uploads/2015/04/style2.png" target="_blank">Style2</a></strong> means that your timer box will show in circle.</li>
													<li><strong>End Date -&gt;</strong> Set days accordingly to you. When you will launch website.</li>
													<li><strong>End Time -&gt;</strong> Set time in hours and minute.</li>
													<li><strong>Auto launch -&gt;</strong> Using this feature will launch automatically launch your website when finish your countdown timer.</li>
													<li><strong>Language String -&gt;</strong> Set Timer label with your own language like (Days, Hours, Minutes and Seconds) for eg. <strong><a title="Language String" href="http://webriti.com/help/wp-content/uploads/2015/04/Language-String.png" target="_blank">Click Here</a></strong>.</li>
												</ul>
												
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-4.jpg"><img class="alignnone size-full wp-image-837" src="http://webriti.com/help/wp-content/uploads/2015/04/new-4.jpg" alt="new-4" width="1390" height="1222"></a></p>
                  
											</div><!-- /.col -->
											
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-5">
										
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to Display Progress Bar</h3>
												<p>Show your work progress for visitor using progress bar. Progress Bar will indicate for user that you are working on your website. According to your work percentage can increase progress bar percentage. You can enter numerical digit without “<strong>%</strong>” symbols. In this feature these setting available End Date and progress Bar Effects.</p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/13.png"><img class="alignnone size-full wp-image-191" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/13.png" alt="13" width="1466" height="451"></a></p>
												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt; &nbsp;Progress Bar Settings<br>
												</strong></p>

												<p>You can see a button. Click on simply ON/OFF button. You will get few setting. See below</p>

												<ul>
													<li><strong>End Date</strong>&nbsp;-&gt; Set your date. If your set date will reached and finished then progress bar hide automatically.</li>
													<li><strong>Percentage Complete Override -&gt; </strong>Set your work percentage digit without trailing % symbols. For eg. Simply enter “55”.<strong><br>
													</strong></li>
													<li><strong>Progress Bar Effects</strong> -&gt; Here is 3 Effects (Basic, Striped and Animated) in progress bar. You can use and enjoy it.</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-5.jpg"><img class="alignnone size-full wp-image-842" src="http://webriti.com/help/wp-content/uploads/2015/04/new-5.jpg" alt="new-5" width="1386" height="791"></a></p>
												
											</div><!-- /.col -->
											
										</div><!-- /.row -->
														
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-6">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Footer Customization?</h3>
												<p>You can display image in footer with URL See below footer settings.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Footer Settings</strong></p>

												<ul>
													<li><strong>Credit Image -&gt;</strong> Upload your image and can check preview using <strong>Preview</strong> button. If You want to remove image click on <strong>Remove Credit</strong>.</li>
													<li><strong>Credit Link -&gt;</strong> Set your own URL on Credit Image. If visitor click on Credit Image it will redirect to Credit Link.</li>
													<li><strong>Credit Image Position -&gt; </strong>You can manage Credit Image position like that Top-Left, Top-Right, Bottom-Left, Bottom-Right. Select your position and click on “<strong>Save Changes</strong>” button.<strong><br>
												</strong></li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-7.jpg"><img class="alignnone size-full wp-image-847" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/new-7.jpg" alt="new-7" width="1390" height="725"></a></p>
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/17.png"><img class="alignnone size-full wp-image-443" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/17.png" alt="17" width="1467" height="636"></a></p>
         
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-7">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Form Setting & Language String.</h3>
												<p>Language String means that you can use your own language like default language is English. If you will enter other language you can do. Change your text for all input fields, notification message and buttons. You can customize easily. After change please don’t forget click on “Save Changes” button.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Language Settings</strong></p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-8.jpg"><img class="alignnone size-full wp-image-850" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/new-8.jpg" alt="new-8" width="1389" height="1038"></a></p>
               
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-8">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to change Template?</h3>
												<p>Plugin has 4 tempaltes find in the template settings. You can change the template any time you want. Each template has it’s own uniqueness. To activate follow below steps.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;</strong><strong>&nbsp;Templates</strong></p>

												<ol>
													<li>You can see 4 template.</li>
													<li>Select any one template.</li>
													<li>After selected template you got a message “<strong>Settings have changed, you should save them!</strong>“</li>
													<li>Now Click on “Save Changes” button. Now your selected template has been changed.</li>
												</ol>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/new-9.jpg"><img class="alignnone size-full wp-image-852" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/new-9.jpg" alt="new-9" width="1391" height="1084"></a></p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/04/20.png"><img class="alignnone size-full wp-image-234" style="border:solid 1px #ccc;" src="http://webriti.com/help/wp-content/uploads/2015/04/20.png" alt="20" width="1466" height="614"></a></p>
                 
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-9">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to Change the Background Color?</h3>
												<p>You can change background color by option panel. When you will select dropdown. you need to select <strong>static background color</strong> option then You will get settings of static background color. Like background color, background effects and overlays effects styles. Set your background color, background effects noise and overlays effects. And don’t forget click on “Save Changes” button.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;&nbsp;Background Settings<br></strong></p>

												<ul>
													<li><strong>Select Background -&gt;</strong>&nbsp;Select Static Background Color</li>
													<li><strong>Background Color -&gt;</strong> <span class="icon help"><span class="tooltip">Select page background color.</span></span></li>
													<li><strong>Background Effects -&gt;</strong> <span class="icon help"><span class="tooltip">Apply noise effects background effect here</span></span>.</li>
													<li><strong>Overlay Effects Styles -&gt;</strong> <span class="icon help"><span class="tooltip">Select Overlay Effects Style. 15 styles available in this option.</span></span></li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-10.jpg"><img class="alignnone size-full wp-image-856" src="http://webriti.com/help/wp-content/uploads/2015/05/new-10.jpg" alt="new-10" width="1388" height="765"></a></p>

												<p>You can see output here of <strong>Background Color </strong>-&gt;</p>
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/24.png"><img class="alignnone size-full wp-image-264" src="http://webriti.com/help/wp-content/uploads/2015/05/24.png" alt="24" width="1467" height="702"></a></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-top-xs inner-left-xs">
												<h3>2. How to Change Background Image?</h3>
												<p>You can change background image by option panel. When you will select dropdown. you need to select&nbsp;static background Image. You will get settings of static background Image. Like background image and cover or stretch. If you will click on <strong>Cover or Stretch </strong>you can see more option of background property.&nbsp; Upload your background image and don’t forget click on “Save Changes” button.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;&nbsp;Background Settings<br></strong></p>

												<ul>
													<li><strong>Select Background -&gt;</strong>&nbsp;Select Static Background Image.</li>
													<li><strong>Background Image-&gt;</strong>&nbsp;<span class="icon help"><span class="tooltip">Select image for coming soon background.<br>
													</span></span></li>
													<li><strong>Cover or Stretch -&gt;</strong> <span class="icon help"><span class="tooltip">Enable background stretch or cover</span></span>. After Enable you will get few settings like that below.
														<ol>
														<li><strong>Background-Repeat -&gt;</strong> <span class="icon help"><span class="tooltip">Choose background repeat properties.</span></span></li>
														<li><strong>Background-Position -&gt;</strong> <span class="icon help"><span class="tooltip">Choose background image position.</span></span></li>
														<li><strong>Background Attachment -&gt;</strong> <span class="icon help"><span class="tooltip">Choose background attachment properties.</span></span></li>
														</ol>
													</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-11.jpg"><img class="alignnone size-full wp-image-861" src="http://webriti.com/help/wp-content/uploads/2015/05/new-11.jpg" alt="new-11" width="1386" height="722"></a></p>
												<p>You can see output here of <strong>Background Image</strong> -&gt;</p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/26.png"><img class="alignnone size-full wp-image-274" src="http://webriti.com/help/wp-content/uploads/2015/05/26.png" alt="26" width="1467" height="702"></a></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-top-xs inner-left-xs">
												<h3>3. How to add a image slideshow background?</h3>
												<p>You can setup background slider by option panel. When you will select dropdown. you need to select&nbsp;static Background Slideshow. Then select of no. slider show. you can select No. of slider. Now you can upload images in slideshow step by step.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;&nbsp;Background Settings<br></strong></p>

												<ul>
													<li><strong>Select Background -&gt;</strong>&nbsp;Select Background Slideshow.</li>
													<li><strong>No. Of Slide-show&nbsp;-&gt;</strong>&nbsp;<span class="icon help"><span class="tooltip">Select number of slide for background slider</span></span>.</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-12.jpg"><img class="alignnone size-full wp-image-863" src="http://webriti.com/help/wp-content/uploads/2015/05/new-12.jpg" alt="new-12" width="1389" height="759"></a></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-top-xs inner-left-xs">
												<h3>4. How to add a Video Background?</h3>
												<p>Play YouTube video in background. You can add video by option panel. When you will select dropdown. you need to select Full-Screen YouTube Background. You will get input field of YouTube Video URL and&nbsp; Paste your You tube URL and click on “Save Changes” button.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;&nbsp;Background Settings<br></strong></p>

												<ul>
													<li><strong>Select Background -&gt;</strong> <span class="icon help"><span class="tooltip">Select Full Screen YouTube Background.</span></span></li>
													<li><strong>YouTube Video URL -&gt;</strong> Enter YouTube URL.</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-13.jpg"><img class="alignnone size-full wp-image-975" src="http://webriti.com/help/wp-content/uploads/2015/05/new-13.jpg" alt="new-13" width="1385" height="718"></a></p>

											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-10">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Text & Color Customization</h3>
												<p>You can change text &amp; color of your entire page. Like heading, description, button, countdown, progress bar, font size and font style. and click on “Save Changes” button.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Text &amp; Color Settings<br></strong></p>

												<ul>
													<li><strong>Headline Font Color&nbsp;-&gt;</strong> <span class="icon help"><span class="tooltip">Select Headline Color.</span></span></li>
													<li><strong>Description Font Color&nbsp;-&gt;</strong>&nbsp;<span class="icon help"><span class="tooltip">Select Description Color.</span></span></li>
													<li><strong>Button, Countdown &amp; Progress bar Color&nbsp;-&gt;</strong>&nbsp;<span class="icon help"><span class="tooltip">Select Button, Countdown Box and Progress Bar&nbsp; background color</span></span>.</li>
													<li><strong>Headline Font size&nbsp;-&gt;&nbsp;</strong><span class="icon help"><span class="tooltip">Select headline font-size</span></span></li>
													<li><strong>Description Font size&nbsp;-&gt;&nbsp;</strong><span class="icon help"><span class="tooltip">Select Description font-size.</span></span></li>
													<li><strong>Button Font size -&gt; </strong><span class="icon help"><span class="tooltip">Select button font size.</span></span></li>
													<li><strong>Headline Font Style&nbsp;-&gt; </strong><span class="icon help"><span class="tooltip">Select headline font style</span></span></li>
													<li><strong>Description Font Style&nbsp;-&gt;</strong> <span class="icon help"><span class="tooltip">Select description font style.</span></span></li>
													<li><strong>Button Font Style&nbsp;-&gt;</strong> <span class="icon help"><span class="tooltip">Select button font style.</span></span></li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-14.jpg"><img class="alignnone size-full wp-image-871" src="http://webriti.com/help/wp-content/uploads/2015/05/new-14.jpg" alt="new-14" width="1386" height="1218"></a></p>

												<p>You can see output here&nbsp; -&gt;</p>
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/30.png"><img class="alignnone size-full wp-image-303" src="http://webriti.com/help/wp-content/uploads/2015/05/30.png" alt="30" width="1600" height="766"></a></p>
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-11">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Container Settings?</h3>
												<p>Enable container settings using ON/OFF button. After enable you will get settings of container like container color, container position (left, right, center), container effects (None, Shadow, Glow), container size (enter in size), container border (enable border with border size in pixel and define border color), border corner radius and container opacity. In this feature you can customize container according to you.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Container Settings<br></strong></p>

												<ul>
													<li><strong>Enable Container -&gt;</strong>You can Enable or Disable Container using this button.</li>
													<li><strong>Container Color -&gt;</strong> <span class="icon help"><span class="tooltip">Choose container background color.</span></span></li>
													<li><strong>Container Position -&gt;</strong> <span class="icon help"><span class="tooltip">Select container background position.</span></span></li>
													<li><strong>Container Effects -&gt; </strong>Select container effects like that none, shadow and glow.</li>
													<li><strong>Container Size -&gt; </strong><span class="icon help"><span class="tooltip">Set container width in Pixel.</span></span></li>
													<li><strong>Container Border -&gt; </strong><span class="icon help"><span class="tooltip">Set container border in Pixel.</span></span></li>
													<li><strong>Container Corner Radius -&gt; </strong>Set container corner radius in pixel.</li>
													<li><strong>Container Opacity -&gt;</strong> Set container opacity.</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-15.jpg"><img class="alignnone size-full wp-image-874" src="http://webriti.com/help/wp-content/uploads/2015/05/new-15.jpg" alt="new-15" width="1387" height="1200"></a></p>

												<p>You can see output here&nbsp; -&gt;</p>
												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/32.png"><img class="alignnone size-full wp-image-310" src="http://webriti.com/help/wp-content/uploads/2015/05/32.png" alt="32" width="1592" height="762"></a></p>

											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-12">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to Use Custom CSS?</h3>
												<p>You can customize with your own CSS styles. You no need any file edit. You can simply paste your CSS Code in editor without style tag. After enter CSS click on “Save Changes” button and see magic on your page.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Custom Codes</strong></p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-16.jpg"><img class="alignnone size-full wp-image-878" src="http://webriti.com/help/wp-content/uploads/2015/05/new-16.jpg" alt="new-16" width="1385" height="723"></a></p>

											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-13">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Social Media Icons</h3>
												<p>You can display social icons with linked your social profiles. Visitor can check your social profile easily. Manage social icons size like small, medium and large.&nbsp;For enable social icons fill the social profile URL and leave blank for disable.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Social Profiles</strong></p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-17.jpg"><img class="alignnone size-full wp-image-882" src="http://webriti.com/help/wp-content/uploads/2015/05/new-17.jpg" alt="new-17" width="1385" height="1549"></a></p>
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-14">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>Notification Settings</h3>
												 <p>In form settings you can use many types of form. Many email marketing tools are available in this form. Like that database, Wp-mail, FeedBurner, MailChimp, Aweber, Campaign Monitor, Constant Contact, Mad Mimi and Get Response. You can use all tools very easy integration. You no need any code you can simply paste your username and API key then will connect automatically. You can see your subscriber and send mail.You can use below&nbsp;these tools.</p>
 
												<p>Go to -&gt;<strong> Notification Settings -&gt; Form Settings</strong></p>
												<ul>
													<li>Database</li>
													<li>Wp-mail</li>
													<li>Feed Burner</li>
													<li>MailChimp</li>
													<li>Aweber</li>
													<li>Campaign Monitor</li>
													<li>Constant Contact</li>
													<li>Mad Mimi</li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-32.jpg"><img class="alignnone size-full wp-image-2032" src="http://webriti.com/help/wp-content/uploads/2015/05/new-32.jpg" alt="new-32" width="1377" height="668"></a></p>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-left-xs">
												<h3>1. Subscriber Form using Database</h3>
												<p>Select Database option using dropdown in form setting. You will get Name field and Enable new subscriber notification. Your visitor enquiry will store in database. If You want display Name field for input? Please check “<strong>Display name field”</strong>. You can check your all subscriber in “Subscriber List” and can take any action (like delete all, delete multiple and export all). You can generate .CSV file of your all subscriber.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Notification Form Settings =&gt; Select Database</strong></p>

												<ul>
													<li><strong>Name Field -&gt;</strong> Select checkbox. If you want to show name field on form field in coming soon page.</li>
													<li><strong>Enable New Subscriber Notification -&gt;</strong> <span class="icon help"><span class="tooltip">Enable user subscriber email notification here.</span></span></li>
												</ul>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-19.jpg"><img class="alignnone size-full wp-image-885" src="http://webriti.com/help/wp-content/uploads/2015/05/new-19.jpg" alt="new-19" width="1387" height="735"></a></p>

												<h3>Where can I see My Subscriber?</h3>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt; Subscriber List<br></strong></p>

												<p>Here you can see you all subscriber. You can export of all subscriber.</p>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/subscriber.png"><img class="alignnone size-full wp-image-1514" src="http://webriti.com/help/wp-content/uploads/2015/05/subscriber.png" alt="subscriber" width="1583" height="738"></a></p>

											</div><!-- /.col -->
											
											<div class="col-md-12 inner-left-xs">
												<h3>2. Subscriber Form using Wp-mail</h3>
												<p>Select Wp-mail option using dropdown in form setting. You will get “<strong>Subscribe Confirm mail for Subscriber</strong><strong>” </strong>and “<strong>Subscriber Admin notify</strong>”. Your visitor email will store in database also will send a welcome mail for visitor and admin. If You want display Name field for input? Please check “<strong>Display name field”</strong>. You can check your all subscriber in “Subscriber List” and can take any action (like delete all, delete multiple and export all). You can generate .CSV file of your all subscriber.</p>

												<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Notification Form Settings =&gt;&nbsp;Select Wp-mail</strong></p>

												<ol>
													<li><strong>Name Field -&gt;</strong> Select checkbox. If you want to show name field on form field in coming soon page.</li>
													<li><strong>Subscribe Confirm mail for Subscriber -&gt;<br>
													</strong><p></p>
														<ul>
														<li>Subject – Subject for Subscriber. You can set subject for Subscriber.</li>
														<li>Body – Body Message for Subscriber. You can set body message for Subscriber.</li>
														</ul>
													</li>
													<li><strong>Subscribe Confirm mail for Subscriber -&gt; </strong>
														<ul>
														<li>Subject – Subject for Admin. You can set subject for Admin.</li>
														<li>Body – Body Message for Admin. You can set body message for Admin.</li>
														</ul>
													</li>
												</ol>

												<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-20.jpg"><img class="alignnone size-full wp-image-888" src="http://webriti.com/help/wp-content/uploads/2015/05/new-20.jpg" alt="new-20" width="1386" height="1349"></a></p>

											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-15">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. Social Media Icons</h3>
												<p> disable.</p>
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									
									
									<div class="tab-content" id="tab-16">
										<div class="row">
											<div class="col-md-12 inner-left-xs">
												<h3>1. How to provide access to white-listed users?</h3>
												<div class="entry-content">
													<p>Access setting means that who user can access or not. In this setting you can set IP and user who accessible. You can remove ip and user. Set your entire website for maintenance mode.&nbsp; You want only separate page for maintenance mode. You can do using <strong>Landing Page Mode</strong>.</p>
													<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Notification Form Settings =&gt;&nbsp;Access Settings </strong></p>
													<ol>
													<li><strong>Access By IP Address</strong> -&gt; <span class="icon help"><span class="tooltip">Add IP Address for user to access the regular site without login</span></span>.</li>
													<li><strong>User Whitelist-&gt;</strong> <span class="icon help"><span class="tooltip">Add User to access the regular site after login</span></span>.</li>
													<li><strong>Landing Page Mode -&gt;</strong> <span class="icon help"><span class="tooltip">Only display the coming soon page on one page of your site and use it as a landing page. Select Entire site option if you want the coming soon page to be display on your entire site.</span></span></li>
													</ol>
													<p><a href="http://webriti.com/help/wp-content/uploads/2015/05/new-28.jpg"><img class="alignnone size-full wp-image-902" src="http://webriti.com/help/wp-content/uploads/2015/05/new-28.jpg" alt="new-28" width="1387" height="1234"></a></p>
													
												</div>
											</div><!-- /.col -->
											
											<div class="col-md-12 inner-left-xs">
												<h3>2. How to Display Coming Soon Page on specific Page / Post?</h3>
												<div class="entry-content">
													<p>You can display Coming Soon Page on specific Page / Post using <strong>Landing Page Mode Setting.</strong> During maintenance&nbsp; you can display coming soon mode for your visitors.</p>

													<p><strong>Go to =&gt; Webriti Coming Soon Pro =&gt;&nbsp;Notification Form Settings =&gt;&nbsp;Access Settings</strong></p>
													
													<p>Here you can see <strong>Landing Page Mode</strong> setting. Select your page / post and click on save changes button. After logged out your WP-admin panel you can see coming soon page.</p>
													
													<p><a href="http://webriti.com/help/wp-content/uploads/2015/06/new-30.jpg"><img class="alignnone size-full wp-image-925" src="http://webriti.com/help/wp-content/uploads/2015/06/new-30.jpg" alt="new-30" width="1386" height="1234"></a></p>

												</div>
											</div><!-- /.col -->
											
										</div>
									</div><!-- /.tab-content -->
									 
								</div><!-- /.panel-container -->
								 
							</div><!-- /.tabs -->
						</div><!-- /.col -->
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
<!-- ============================================================= SECTION – ACCORDION : END ============================================================= -->
			

<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>