<?php 
/*
 * This template file for displaying footer.
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
?>
		<!-- ============================================================= FOOTER ============================================================= -->
		
		<footer class="footer-sidebar">
			<div class="container inner">
				<div class="row">
					
					<div class="col-md-3 col-sm-6 inner">
						<?php 
						if ( is_active_sidebar( 'footer1' ) ) :
							dynamic_sidebar( 'footer1' );
						endif;
						?>
					</div>
					
					<div class="col-md-3 col-sm-6 inner">
						<?php 
						if ( is_active_sidebar( 'footer2' ) ) :
							dynamic_sidebar( 'footer2' );
						endif;
						?>
					</div>
					
					<div class="col-md-3 col-sm-6 inner">
						<?php 
						if ( is_active_sidebar( 'footer3' ) ) :
							dynamic_sidebar( 'footer3' );
						endif;
						?>
					</div>
					
					<div class="col-md-3 col-sm-6 inner">
						<?php 
						if ( is_active_sidebar( 'footer4' ) ) :
							dynamic_sidebar( 'footer4' );
						endif;
						?>
					</div>
						
				</div><!-- /.row --> 
			</div><!-- .container -->
		  
			<div class="site-info">
				<div class="container inner">
					<p class="pull-left"><?php _e('© 2016 Coming Soon. All rights reserved.','ecs'); ?></p>
					<?php 
					wp_nav_menu( array(
							'theme_location' => 'footer',
							'container'  => '',
							'menu_class' => 'footer-menu pull-right',
							'fallback_cb' => 'webriti_fallback_page_menu',
							'walker' => new webriti_nav_walker()) 
							); 
					?>
				</div><!-- .container -->
			</div><!-- .footer-bottom -->
		</footer>
		
		<!-- ============================================================= FOOTER : END ============================================================= -->
	<?php wp_footer(); ?>
	</body>

</html>
