=== Easy Coming Soon ===
Contributors: Webriti
Requires at least: WordPress 4.4
Tested up to: WordPress 4.7.4
Version: 0.2

== Installation ==
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Easy Coming Soon in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==
Easy Coming Soon WordPress Theme, Copyright 2014-2015 WordPress.org
Easy Coming Soon is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Notes ==
Only the default and dark color schemes are accessibility ready.

== Change Log ==


1.3 ===
1. Added Template 5 (template-premium-template.php)

1.2 ===
1. Extra jquery script removed from scripts.js file.
2. Adding 2 files for home page tab and owl slider.

1.1 ===
1. Added New Testimonials Review

1.0 ===
1. update pricing template link.

0.1 ===
checkout page and login page styled.
