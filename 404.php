<?php 
/*
 * This is main template file for 404 page error message
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

<section id="blog" class="light-bg">
	<div class="container inner-top-sm inner-bottom">
					
		<div class="row">		
			<div class="col-md-8">
				<div class="site-content">
				
					<header class="entry-header">
						<h2 class="entry-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'ecs' ); ?></h2>
					</header>
					
					<div class="entry-content">
						<p><?php _e( 'It looks like nothing was found at this location. May be try a search?', 'ecs' ); ?></p>

						<?php get_search_form(); ?>
					</div><!-- .entry-content -->

				</div><!-- /.site-content -->
							
			</div><!-- /.col -->
			
			<?php get_sidebar(); ?>
						
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>