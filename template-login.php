<?php 
/*
 * Template Name: Login Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

<!--content wrapper-->
	<div class="signin"> 
	  
		<!--Ecs signup form-->
		<section class="ecs-form3 ecs-light section-spacing3">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 center-block">
						<?php 
						while ( have_posts() ) : the_post();
						the_content();
						endwhile;
						?>
					</div>
				</div>
			</div>
		</section>
	    <!--Ecs signup form end--> 
	  
	</div>
	<!--content wrapper end-->

<?php get_footer(); ?>