<?php 
/*
 * Template Name: Features Page with Images
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
<section id="services">
				<div class="container inner-top inner-bottom-sm">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 inner-bottom-sm center-block text-center">
							<header>
								<h1>Plugin Features</h1>
								<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor. Laceaque quiae sitiorem rest non restibusaes maio es dem tumquam.</p>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row feature3 feature-bottom">
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-2.jpg">
								</figure>
								<div class="features-info">
									<h3>MailChimp Ready</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-1.jpg">
								</figure>
								<div class="features-info">
									<h3>Mobile Friendly</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-5.jpg">
								</figure>
								<div class="features-info">
									<h3>Instant Notification</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						<div class="clearfix"></div>
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-2.jpg">
								</figure>
								<div class="features-info">
									<h3>MailChimp Ready</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-1.jpg">
								</figure>
								<div class="features-info">
									<h3>Mobile Friendly</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						
						<div class="col-sm-4 inner-bottom-xs">
							<aside class="features">
								<figure class="post-thumbnail">
									<img src="<?php echo get_template_directory_uri(); ?>/images/feature-5.jpg">
								</figure>
								<div class="features-info">
									<h3>Instant Notification</h3>
									<div class="separator"></div>
									<p>Magnis modipsae que lib volor porem aut labore Laceaque quiae sitiorem rest non restibus es  tumquam core posae volor modis volor.</p>
								</div>
							</aside>
						</div>
						<div class="clearfix"></div>
						
					</div><!-- /.row -->
									
				</div><!-- /.container -->
			</section>
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>