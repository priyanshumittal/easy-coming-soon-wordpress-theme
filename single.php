<?php
/**
 * The template part for displaying post detail
 *
 */

get_header(); ?>

<section id="blog" class="light-bg">
	<div class="container inner-top-sm inner-bottom">
					
		<div class="row">		
			<div class="col-md-8">
				<div class="site-content">
				
				<?php if ( have_posts() ) : ?>
			
					<?php 
					while ( have_posts() ) : the_post();
						get_template_part( 'content', '' );
						
						// If comments are open or we have at least one comment
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
						
					endwhile;
					?>
			
				<?php endif; ?>

				</div><!-- /.posts -->
							
			</div><!-- /.col -->
			
			<?php get_sidebar(); ?>
						
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>