<?php
/**
 * The template part for sidebar
 *
 */
?>
<div class="col-md-4 inner-left-sm">
	<div class="sidebar">
		<?php 
			if ( is_active_sidebar( 'sidebar_primary' ) ) :
				dynamic_sidebar( 'sidebar_primary' );
			endif;
		?>			
	</div>
</div>