<?php
/**
 * Template for displaying search forms
 *
 */
?>

<form role="search" method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<input class="search-field" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'ecs' ); ?>">
		<input type="submit" class="btn-small" value="Search">
	</div>
</form>
