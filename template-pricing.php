<?php 
/*
 * Template Name: Pricing Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
<!-- ============================================================= SECTION – PRICING TABLES Top Center ====================================================== -->
			
			<section id="pricing-tables">
				<div class="container inner">
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Easy Coming Soon Pro</h1>
								<!--<p>Without the High Costs</p>-->
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row pricing2">
						<div class="col-md-6 col-md-offset-3">
							
							<div class="col-md-6">
								<div class="plan">
									
									<header>
										<h2>Personal Plan</h2>
										<span class="price1"><span><sup>$</sup>29</span></span>
										<span class="duration"><b>Single Site License</b></span>
										<a class=" btn btn-dark" href="https://www.member.easycomingsoon.com/checkout/?&edd_action=add_to_cart&download_id=25"><i class="fa fa-download"></i>Buy Now</a>
									</header>
									
									<ul class="feature">
									
										<li>+ Access Controls</li>
										<li>+ Social Sharing</li>
										<li>+ Google Fonts</li>
										<li>+ Background Image Slideshow</li>
										<li>+ YouTube Video Background</li>
										<li>+ Social Links</li>
										<li>+ Email Marketing & Data Capture</li>
										<li>+ Subscriber List</li>
										<li>+ Progress Bar</li>
										<li>+ Countdown Timer</li>
										<li><b>+ 1 Year Support</b></li>
										<li><b>+ 1 Year Updates</b></li>
										<li><a href="http://easycomingsoon.com/themes">+ Access to all Themes</a></li>
										<li><a title="See all the features..." href="http://easycomingsoon.com/features/">See all the features...</a></li>
									</ul><!-- /.features -->
									
								</div><!-- /.plan -->
							</div>
						
							<div class="col-md-6">
								<div class="plan">
									
									<header>
										<span class="most-popular">Most Popular</span>
										<h2>Developer Plan</h2>
										<span class="price1"><span><sup>$</sup>59</span></span>
										<span class="duration"><b>Unlimited Site License</b></span>
										<a class=" btn btn-mandy" href="https://www.member.easycomingsoon.com/checkout/?&edd_action=add_to_cart&download_id=27"><i class="fa fa-download"></i>Buy Now</a>
									</header>
									
									<ul class="feature">
										<li>+ Access Controls</li>
										<li>+ Social Sharing</li>
										<li>+ Google Fonts</li>
										<li>+ Background Image Slideshow</li>
										<li>+ YouTube Video Background</li>
										<li>+ Social Links</li>
										<li>+ Email Marketing & Data Capture</li>
										<li>+ Subscriber List</li>
										<li>+ Progress Bar</li>
										<li>+ Countdown Timer</li>
										<li><b>+ 1 Year Support</b></li>
										<li><b>+ 1 Year Updates</b></li>
										<li><a href="http://easycomingsoon.com/themes">+ Access to all Themes</a></li>
										<li><a title="See all the features..." href="http://easycomingsoon.com/features/">See all the features...</a></li>
									</ul><!-- /.features -->
									
								</div><!-- /.plan -->
							</div>
						
						
							<div class="clearfix"></div>
							
							
							
						</div>
						
						

					</div><!-- /.row -->
					
					
				</div><!-- /.container -->
			</section>
			
			<!-- ============================================================= SECTION – PRICING TABLES ONE : END ============================================================= -->
			
		
			
			<!-- ============================================================= SECTION – FAQ ============================================================= -->
			
			<section id="faq" class="light-bg">
				<div class="container inner">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Frequently Asked Questions</h1>
								<p></p>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row">
					
						<div class="col-sm-6 inner-right-sm inner-top-sm inner-bottom-sm">
							
							<h3>Do I need coding skills to use Easy Coming Soon Plugin?</h3>
							<p>Absolutely not. You can easily create coming soon page without any coding knowledge.</p>
							
							<h3>I have some Pre Sales queries. How do I contact you?</h3>
							<p>Click <a href="http://easycomingsoon.com/contact-us/" title="Contact Here">here</a> to contact us with Pre Sales queries.</p>
							
							<h3>Do you offer Refunds?</h3>
							<p>Generally, no; only in some very special situations. You can find more information about the refund policy on the <a href="http://easycomingsoon.com/terms-and-conditions/" title="Terms and Conditions">terms and conditions page </a>.</p>
							
						</div><!-- /.col -->
					
						<div class="col-sm-6 inner-left-sm inner-top-sm inner-bottom-sm">
							
							<h3>How is support provided?</h3>
							<p>For support simple contact us via this <a href="http://easycomingsoon.com/contact-us/" title="Contact Here">form</a></p>
							
							<h3>What types of payment methods are accepted?</h3>
							<p>Currently we accept Visa, Mastercard, Discover, American Express & PayPal.</p>
							
							<h3>Will Easy Coming Soon quit working if my license expires?</h3>
							<p>NO. Easy Coming Soon will work just fine but you will not have access to product support, documentation and updates.</p>
							
						</div><!-- /.col -->
						
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
			<!-- ============================================================= SECTION – FAQ : END ============================================================= -->
		
	
<?php get_footer(); ?>