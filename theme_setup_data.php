<?php
function theme_data_setup()
{	
	return $theme_options = array(
			'logo' => get_template_directory_uri().'/images/logo.png',
			'contact_title'=>__('Contact Us','ecs'),
			'contact_desc'=>__('Have questions? We’re happy to help!','ecs'),
			'signup_text'=>__('Sign up','ecs'),
			'signup_url'=>'#',
			'login_text'=>__('Login','ecs'),
			'login_url'=>'#',
		);
}