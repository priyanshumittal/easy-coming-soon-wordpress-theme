<?php 
/*
 * Template Name: Demo Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
<section id="latest-works" class="portfolio">
	<div class="container inner">
		
		<div class="row">
			<div class="col-md-8 col-sm-9 center-block inner-bottom-sm text-center">
				<header>
					<h1>Coming Soon Pro Demo</h1>
					<!--<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor.</p>-->
				</header>
			</div>
		</div><!-- /.row -->
	 
		<div class="row">
			<div class="col-sm-3 inner-bottom-sm">
				<a href="http://easycomingsoon.com/demo/image-background/" target="_blank" class="demo-view shadow">
					<figure class="post-thumbnail">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo1.jpg" alt="img">
						<div class="thumbnail-showcase-overlay">
							<div class="thumbnail-showcase-overlay-inner">
								<div class="description">
									<p class="txt-white">Attract new visitors, increase sales, and improve customer retention. Marketing Apps and services help drive traffic.</p>
								</div>
							</div>
						</div>
					</figure>	
					<div class="demo-content"><h4>Image Background</h4></div>	
				</a>
			</div>
			
			<div class="col-sm-3 inner-bottom-sm">
				<a href="http://easycomingsoon.com/demo/background-slider/" target="_blank" class="demo-view shadow">
					<figure class="post-thumbnail">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo2.jpg" alt="img">
						<div class="thumbnail-showcase-overlay">
							<div class="thumbnail-showcase-overlay-inner">
								<div class="description">
									<p class="txt-white">Attract new visitors, increase sales, and improve customer retention. Marketing Apps and services help drive traffic.</p>
								</div>
							</div>
						</div>
					</figure>	
					<div class="demo-content"><h4>Image Slider Background</h4></div>	
				</a>
			</div>
			
			<div class="col-sm-3 inner-bottom-sm">
				<a href="http://easycomingsoon.com/demo/background-video/" target="_blank" class="demo-view shadow">
					<figure class="post-thumbnail">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo3.jpg" alt="img">
						<div class="thumbnail-showcase-overlay">
							<div class="thumbnail-showcase-overlay-inner">
								<div class="description">
									<p class="txt-white">Attract new visitors, increase sales, and improve customer retention. Marketing Apps and services help drive traffic.</p>
								</div>
							</div>
						</div>
					</figure>	
					<div class="demo-content"><h4>YouTube Video Background</h4></div>	
				</a>
			</div>
			
			<div class="col-sm-3 inner-bottom-sm">
				<a href="http://easycomingsoon.com/demo/test-install/wp-login.php" target="_blank" class="demo-view shadow">
					<figure class="post-thumbnail">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo4.jpg" alt="img">
						<!--<div class="thumbnail-showcase-overlay">
							<div class="thumbnail-showcase-overlay-inner">
								<div class="description">
									<p class="txt-white"><strong>User Login Detail</strong><br><br><b>Username:</b> user <br> <b>Password:</b> user</p>
								</div>
							</div>
						</div>-->
					</figure>	
					<!--<div class="demo-content"><h4>View Demo</h4></div>-->	
				</a>
			</div>
			
		</div>

	</div><!-- /.container -->
</section>
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>