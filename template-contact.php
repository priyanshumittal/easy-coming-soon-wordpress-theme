<?php 
/*
 * Template Name: Contact Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header();
$current_options = wp_parse_args(  get_option( 'ecs_option', array() ), theme_data_setup() ); ?>

<section id="contact-form">
				<div class="container inner">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<?php if($current_options['contact_title']!=''): ?>
								<h1><?php echo $current_options['contact_title']; ?></h1>
								<?php endif; ?>
								
								<?php if($current_options['contact_desc']!=''): ?>
								<p><?php echo $current_options['contact_desc']; ?></p>
								<?php endif; ?>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								
								<div class="col-sm-6 outer-top-md inner-right-sm">
									
									<?php 
									while ( have_posts() ) : the_post();
										the_content(); 
									endwhile;
									?>
									
								</div><!-- ./col -->
								
								<div class="col-sm-6 outer-top-md inner-left-sm border-left">
									
									<?php 
									if ( is_active_sidebar( 'contact' ) ) :
									dynamic_sidebar( 'contact' );
									endif;
									?>
									
								</div><!-- /.col -->
								
							</div><!-- /.row -->
						</div><!-- /.col -->
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>

<?php get_footer(); ?>