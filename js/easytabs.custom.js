/*===================================================================================*/
/*	TABS
/*===================================================================================*/
jQuery(document).ready(function ($) {
	
	$('').easytabs({
		cycle: 1500
	});
	
	$('.tabs.tabs-top, .tabs.tabs-circle-top, .tabs.tabs-2-big-top, .tabs.tabs-side').easytabs({
		animationSpeed: 0,
		updateHash: false
	});
	
});