<?php 
/*
 * Template Name: Testimonials Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
			
	<!-- ============================================================= SECTION – TESTIMONIAL ============================================================= -->
	<section id="testimonial">
		<div class="container inner">
			
			<div class="row">
				<div class="col-md-8 col-sm-9 center-block inner-bottom-sm text-center">
					<header>
						<h1>What our clients talk about us?</h1>
						<!--<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor.</p>-->
					</header>
				</div>
			</div><!-- /.row -->
		 
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-1.jpeg">
							</aside>
							<div class="media-body">
								
								<p class="user-description">"This plugin is great especially if you want to work behind the scene with any theme. We experienced a problem with this plugin and the creators fixed it within a day. Easy to use, very flexible and strongly recommended."</p>
								<h4 class="user-name"><span class="user-designation">- DaiAku</span></h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-7.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Does exactly what it is meant to do. You can add your own background image and configure your text. You can also use links to your social sites and offer a subscription form. It's a great plugin that works very well."</p>
								<h4 class="user-name"><span class="user-designation">- Michael Cobb</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-3.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"This is a really great plugin with very nice features in the free version. I love how boldly it displays your site information. It is better than the other plugin I had been using for years."</p>
								<h4 class="user-name"><span class="user-designation">- StanleyDay</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-10.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"This is a great plug-in. I’m a non-techie, and I easily installed and customized my Coming Soon page. I like the option of choosing my own background and the live preview. Very nice."</p>
								<h4 class="user-name"><span class="user-designation">- Nonprofit Coach</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-2.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"I am a WP newbie and wanted a "coming soon" landing page that was very easy to setup while I got the website ready. This was great!."</p>
								<h4 class="user-name"><span class="user-designation">- Wdwilliams86</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-4.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Set up in no time. No more worries about developing the site live and showing all my mistakes during the proces."</p>
								<h4 class="user-name"><span class="user-designation">- Kukelstaart</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-8.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"Quick and easy to configure, clean interface and great customising features. Outstanding simplicity. Thanks."</p>
								<h4 class="user-name"><span class="user-designation">- 2bears</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-6.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Easy to setup and configure. Makes the Coming Soon page look great with a custom background image. 5-stars."</p>
								<h4 class="user-name"><span class="user-designation">- Sennhauser</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-11.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Nice limited free version. Just the minimum enough to set up a coming soon page to your liking. So far, so good... :)"</p>
								<h4 class="user-name"><span class="user-designation">- CoolFede</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-9.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"This plugin is straightforward and works as expected. I like being able to choose a background picture."</p>
								<h4 class="user-name"><span class="user-designation">- Jim Skintauy</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-13.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"A simple and easy to use plug-in that works and does what it should. Nice features without having to pay for it. Can definitely recommend it."</p>
								<h4 class="user-name"><span class="user-designation">- BeetgeMedia</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-12.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Very easy to set up and use. Am using on two sites I am developing."</p>
								<h4 class="user-name"><span class="user-designation">- Andrew Newman</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-5.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"Great work guys. Does exactly what you say, and looks really sharp!"</p>
								<h4 class="user-name"><span class="user-designation">- David Gadarian</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-14.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Thank you for a handy quick to install plugin to put up an "Under Construction" page!"</p>
								<h4 class="user-name"><span class="user-designation">- Kris Argent </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-15.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Simple but powerful enough. First plugin to install on a new website."</p>
								<h4 class="user-name"><span class="user-designation">- Femorais</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-16.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"Nice Layouts, many choices. easy to setup. 5 stars."</p>
								<h4 class="user-name"><span class="user-designation">- Sblinnin</span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-17.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Thank you for the plugin :)"</p>
								<h4 class="user-name"><span class="user-designation">- Marcoghirello </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/no-image.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"Great to have a splashpage for the time being!"</p>
								<h4 class="user-name"><span class="user-designation">- Ottowijnen </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/no-image.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"Really, great plugin that i used with all my sites. Thanks for the excellent support."</p>
								<h4 class="user-name"><span class="user-designation">- Ashraf Jibrael </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-19.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Plugin does exactly what I want it to do. it' works perfectly. Thanks for your help...supports is amazing. Love the plugin."</p>
								<h4 class="user-name"><span class="user-designation">- Queensratchet </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/user-20.jpeg">
							</aside>
							<div class="media-body">
								<p class="user-description">"Very simple. Very dependable. Quick and easy. That is all I could ask for. Thanks much."</p>
								<h4 class="user-name"><span class="user-designation">- Steve </span></h4>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="ecs-testimonial">
						<div class="media">
							<aside class="ecs-user">
								<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial/no-image.png">
							</aside>
							<div class="media-body">
								<p class="user-description">"I needed the support help, it was fast and nice. In a short time they have solved all the technical problems. thank you very much"</p>
								<h4 class="user-name"><span class="user-designation">- Terribit </span></h4>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div><!-- /.container -->
	</section>
	<!-- ============================================================= SECTION – TESTIMONIAL : END ============================================================= -->
	
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>