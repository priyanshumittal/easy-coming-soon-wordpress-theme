<?php 
/*
 * This is main template file of ecs theme.
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

<section id="blog" class="light-bg">
	<div class="container inner-top-sm inner-bottom">
					
		<div class="row">		
			<div class="col-md-8">
				<div class="site-content">
				
				<?php if ( have_posts() ) : ?>
					
					<header class="entry-header">
						<?php
							the_archive_title( '<h2 class="entry-title">', '</h2>' );
							the_archive_description( '<p class="taxonomy-description">', '</p>' );
						?>
					</header>
					
					<?php 
					while ( have_posts() ) : the_post();
						get_template_part( 'content', '' );
					endwhile;
					?>
				<?php else :?>
				<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

				</div><!-- /.posts -->
				
				<?php 
				if ( have_posts() ) :
					// pagination function
					$obj = new Webriti_pagination();
					$obj->Webriti_page();
				endif;
				?>
							
			</div><!-- /.col -->
			
			<?php get_sidebar(); ?>
						
		</div><!-- /.row -->
	</div><!-- /.container -->
</section>

<?php get_footer(); ?>