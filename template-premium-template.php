<?php 
/*
 * Template Name: Premium Template Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
<section id="latest-works" class="portfolio">
	<div class="container inner">
		
		<div class="row">
			<div class="col-md-8 col-sm-9 center-block inner-bottom-sm text-center">
				<header>
					<h1>Premium Coming Soon Templates</h1>
					<!--<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor.</p>-->
				</header>
			</div>
		</div><!-- /.row -->
	 
		<div class="row">
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-1.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 1</h4></div>	
			</div>
			
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-2.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 2</h4></div>	
			</div>
			
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-3.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 3</h4></div>	
			</div>
			
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-4.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 4</h4></div>	
			</div>
			
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-5.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 5</h4></div>	
			</div>
			
			<div class="col-sm-4 inner-bottom-sm">
				<figure class="post-thumbnail">
					<img src="<?php echo get_template_directory_uri(); ?>/images/template/template-6.jpg" alt="img">
				</figure>	
				<div class="demo-content"><h4>Premium Template 6</h4></div>	
			</div>
			
		</div>

	</div><!-- /.container -->
</section>
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>