<?php 
/*
 * Template Name: Home Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

<section class="slide-bg">
			<div class="slide-bg-overlay">
				<div class="container">
						<div class="col-md-12">
							<div class="caption text-center inner-right-xs">
								<h1 class="txt-white title">Easy Coming Soon - Pro</h1>
								<p class="description">Quickly build and Launch Beautiful looking Landing Page for your website. Ideal for creating Landing Pages, Coming Soon Pages and Under Construction pages. Collect visitor emails and engage with them using Newsletter integration. Use on any number of sites.</p>
								<div class="btn-block">
									<a class="btn btn-large btn-green font-18" href="http://easycomingsoon.com/features/">View Features</a>
									<a class="btn btn-large btn-white font-18" href="http://easycomingsoon.com/demos/">See Demo</a>
								</div>	
							</div>
						</div>
						
						<div class="col-md-12">
							<figure><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mockup-1.png" alt=""></figure>
						</div>
					</div>
				</div>	
			</section>
			
			<!-- ============================================================= SECTION – HERO : END ============================================================= -->
			
			
			<!-- ============================================================= SECTION – FEATURES ============================================================= -->
			
			<section id="services">
				<div class="container inner-top inner-bottom-sm">
					
					<!-- /.row -->
					
									
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Plugin Features</h1>
								<p>Over 200,000 downloads and counting...</p>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row inner-top-sm text-center">
					
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-edit"></i>
							</div><!-- /.icon -->
							<h3>Realtime Page Customizer</h3>
							<p class="text-small">Just like WordPress Theme Customizer plugin has in-built realtime page Customizer, helps you in creating coming soon pages in a flash.</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-list"></i>
							</div><!-- /.icon -->
							<h3>Multiple Design Templates</h3>
							<p class="text-small">Plugin comes with 6 pre-defined templates which act as a great starting point for your landing page.</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-mobile"></i>
							</div><!-- /.icon -->
							<h3>Mobile Friendly</h3>
							<p class="text-small">All templates are Responsive out of the box. Landing pages display perfectly on mobile and tablet devices.</p>
						</div><!-- /.col -->
						
					</div><!-- /.row -->
					
					<div class="row text-center">
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-lock"></i>
							</div><!-- /.icon -->
							<h3>Access Control</h3>
							<p class="text-small">Configure which users can view the live site. White List IP and Users. This feature is perfect to share the website privately with the client.</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-database"></i>
							</div><!-- /.icon -->
							<h3>Subscriber DataBase</h3>
							<p class="text-small">Quickly capture and store visitor email address in wordpress database. You can also export the email address</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-envelope-o"></i>
							</div><!-- /.icon -->
							<h3>Newsletter Integration</h3>
							<p class="text-small">Quickly Integrate with Autoresponser Services like Mailchimp, Campaign Monitor, Aweber etc.</p>
						</div><!-- /.col -->
						
					</div><!-- /.row -->
					
					
					
				
						
					
						
						
					</div>	
					
					
					
<!-- /.row -->
					

						
					</div><!-- /.row -->
				<div class="row text-center">
						
						<div class="btn-wrapper">
							<a href="http://easycomingsoon.com/features/" class="btn btn-mandy">View All Features</a>
						</div>
						
					</div>
			</section>
			
			<!-- ============================================================= SECTION – REASONS ============================================================= -->
			
			<section id="reasons">
				<div class="container inner">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Why Select Easy Coming Soon?</h1>
								<!--<p>Doloreiur quia commolu dolupta oreprerum tibusam.</p>-->
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row inner-top-sm">
						<div class="col-xs-12">
							<div class="tabs tabs-reasons tabs-circle-top tab-container">
								
								<ul class="etabs text-center">
									<li class="tab"><a href="#tab-1"><div><i class="fa fa-check-square-o"></i></div>Quick Setup</a></li>
									<li class="tab"><a href="#tab-2"><div><i class="fa fa-database"></i></div>Lead Management</a></li>
									<li class="tab"><a href="#tab-3"><div><i class="fa fa-clock-o"></i></div>Access Control</a></li>
									<li class="tab"><a href="#tab-4"><div><i class="fa fa-share-square"></i></div>Design Settings</a></li>
									<li class="tab"><a href="#tab-5"><div><i class="fa fa-envelope-o"></i></div>Count Down Timer</a></li>
								</ul><!-- /.etabs -->
								
								<div class="panel-container">
									
									<div class="tab-content" id="tab-1">
										<div class="row">
											
											<div class="col-md-5 col-md-push-5 col-md-offset-1 col-sm-6 col-sm-push-6 inner-left-xs">
												<figure><img style="border: solid 1px #ccc;" src="<?php echo get_stylesheet_directory_uri(); ?>/images/customizer.gif" alt=""></figure>
											</div><!-- /.col -->
											
											<div class="col-md-5 col-md-pull-5 col-sm-6 col-sm-pull-6 inner-top-xs inner-right-xs">
												<h3>Quick Setup</h3>
												<p>Spend more time in promoting your site and less time in designing a launch page. Plugin comes with 6 built in templates so that you can quickly launch your site in minutes</p>
											</div><!-- /.col -->
											
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-2">
										<div class="row">
											
											<div class="col-md-8 col-sm-9 center-block text-center">
												<h3>Store Email Address </h3>
												<p> Capturing Email address is THE most important function of a landing page. Capture Email address and store in Wordpress database. Connect with various newsletter providers and automatically push new subscribers to email service of your choice. </p>
												<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/art/marketing.jpg" alt=""></figure>
											</div><!-- /.col -->	
											
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-3">
										<div class="row">
											<div class="col-md-8 col-sm-9 center-block text-center">
												<h3>Share the Website with Select Users</h3>
												<p> So you want to share the website with the client while keeping it private. It is possible with the access control and whitelisting feature. Simply create account for a user and white list him. He will be able to login and access the complete website. </p>
												
											</div><!-- /.col -->	
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-4">
										
									
										
										<div class="row">
											<div class="col-sm-8 center-block text-center">
												<h3>Powerful Design Settings</h3>
												<p> Customize fonts , colors and backgorund with ease. Add a Slider Backgorund, Video Background from the Dashboard. No coding necessary  </p>
											</div><!-- /.col -->
										</div><!-- /.row -->
										
									</div><!-- /.tab-content -->
									
									<div class="tab-content" id="tab-5">
										<div class="row">
											<div class="col-md-8 col-sm-9 center-block text-center">
												<h3>Display A Count Down Timer</h3>
												<p>Display a countdown timer and a progress bar to users. Set a launch date and lets progress display the time left to launch  </p>
												<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/art/countdown.jpg" alt=""></figure>
												</div><!-- /.col -->
										</div><!-- /.row -->
									</div><!-- /.tab-content -->
									 
								</div><!-- /.panel-container -->
								 
							</div><!-- /.tabs -->
						</div><!-- /.col -->
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
			<!-- ============================================================= SECTION – REASONS : END ============================================================= -->
						<!-- ============================================================= SECTION – TESTIMONIALS ============================================================= -->
			
			<section id="testimonials" class="light-bg img-bg-softer">
				<div class="container inner">
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Our Client Speaks</h1>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<div id="owl-testimonials" class="owl-carousel owl-outer-nav owl-ui-md">
								
								<div class="item">
									<blockquote>
										<p><b>Great Plugin - Excellent Support!!!</b> This plugin is great especially if you want to work behind the scene with any theme. We experienced a problem with this plugin and the creators fixed it within a day. Easy to use, very flexible and strongly recommended.</p>
										<footer>
											<cite>Daiku</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p>This is a really great plugin with very nice features in the free version. I love how boldly it displays your site information. It is better than the other plugin I had been using for years.<p>
										<footer>
											<cite>Stanley Day</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p>Really the best of it kind and I have been around the block... Super Well DONE!!!</p>
										<footer>
											<cite>King Beven</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p><b>Very Good Plugin</b> this works exactly as advertised. There nothing that goes 'above and beyond' without the $39 fee.</p>
										<footer>
											<cite>Mister Tom</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p><b>Very Simple!!!</b> I'm actually using this plugin in two sites. Very simple to use.
Thx for the work !</p>
										<footer>
											<cite>Center Web</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p><b>Simple and Straightforward!!!</b> Does exactly what it is meant to do. You can add your own background image and configure your text. You can also use links to your social sites and offer a subscription form. It's a great plugin that works very well.</p>
										<footer>
											<cite>Michael Cobb</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								<div class="item">
									<blockquote>
										<p><b>Really great, the best one around!!!</b> thank you! I've tried a few others, but this one tops it all. You can have beautiful image as background & email capture & social buttons too.</p>
										<footer>
											<cite>Lane</cite>
										</footer>
									</blockquote>
								</div><!-- /.item -->
								
								
							</div><!-- /.owl-carousel -->
						</div><!-- /.col -->
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
			<!-- ============================================================= SECTION – TESTIMONIALS : END ============================================================= -->
			
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>