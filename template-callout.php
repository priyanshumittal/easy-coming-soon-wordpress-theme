<section id="get-started" class="tint-bg callout">
				<div class="container inner-sm">
					<div class="row">
						<div class="col-sm-11 center-block text-center">
							<h1 class="single-block">Create beautiful & responsive coming soon page in minutes <a href="http://member.easycomingsoon.com/pricing" class="btn btn-large font-18"><i class="fa fa-wordpress"></i>Buy Now!</a></h1>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			</section>