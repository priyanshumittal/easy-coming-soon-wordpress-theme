<?php 
/*
 * Template Name: About Us Page
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>

		
			
			<!-- ============================================================= SECTION – WHO WE ARE ============================================================= -->
			
			

			<section id="team" class="light-bg">
				<div class="container inner-top inner-bottom-sm">
					
					<div class="row">
						<div class="col-md-8 col-sm-10 center-block text-center">
							<header>
								<h1>The people behind the magic</h1>
								<p>We are all a little different in our own ways – yet together we flourish because of those contrasting talents, interests & personalities.</p>
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row inner-top-sm text-center">
						
						<div class="col-sm-4 inner-bottom-sm inner-left inner-right">
							<figure class="member">
								
								<div class="icon-overlay icn-link">
									<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/team/ankit_sir.jpg" class="img-circle"></a>
								</div><!-- /.icon-overlay -->
								
								<figcaption>
								
									<h2>
										Ankit Agarwal
										<span>Founder</span>
									</h2>
									
									
									
								</figcaption>
								
							</figure>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-sm inner-left inner-right">
							<figure class="member">
								
								<div class="icon-overlay icn-link">
									<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/team/priyanshu_sir.jpg" class="img-circle"></a>
								</div><!-- /.icon-overlay -->
								
								<figcaption>
								
									<h2>
										Priyanshu Mittal
										<span>Founder</span>
									</h2>
									
									
									
								</figcaption>
								
							</figure>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-sm inner-left inner-right">
							<figure class="member">
								
								<div class="icon-overlay icn-link">
									<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/team/mouse.jpg" class="img-circle"></a>
								</div><!-- /.icon-overlay -->
								
								<figcaption>
								
									<h2>
										Our Stuart
										<span>Our Landlord</span>
									</h2>
									
									
									
								</figcaption>
								
							</figure>
						</div><!-- /.col -->
						
					</div><!-- /.row -->
					
				</div><!-- /.container -->
			</section>
			
			<!-- ============================================================= SECTION – TEAM : END ============================================================= -->
			
			<!-- ============================================================= SECTION – WHO WE ARE : END ============================================================= -->
		

			
<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>