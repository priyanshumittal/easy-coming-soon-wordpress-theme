<?php 
/*
 * This template file for displaying header.
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
window.__insp = window.__insp || [];
__insp.push(['wid', 523048364]);
(function() {
function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
})();
</script>
<!-- End Inspectlet Embed Code -->
		<!-- Meta -->
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<title><?php wp_title( '|' , true , 'right' ); ?></title>
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
	<?php $current_options = wp_parse_args(  get_option( 'ecs_option', array() ), theme_data_setup() ); ?>
		
		<!-- ============================================================= HEADER ============================================================= -->
		
		<header>
			<div class="navbar">
				
				<div class="navbar-header">
					<div class="container">
						<!-- ============================================================= LOGO MOBILE ============================================================= -->
						<?php if( $current_options['logo'] != '' ): ?>
						<a class="navbar-brand" href="http://easycomingsoon.com">
							<img src="<?php echo $current_options['logo']; ?>" class="logo" alt="<?php bloginfo('name'); ?>">
						</a>
						<?php endif; ?>
						
						<!-- ============================================================= LOGO MOBILE : END ============================================================= -->
						
						<a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a>
						
					</div><!-- /.container -->
				</div><!-- /.navbar-header -->
				
				<div class="yamm">
					<div class="navbar-collapse collapse">
						<div class="container">
							
							<!-- ============================================================= LOGO ============================================================= -->
							
							<?php if( $current_options['logo'] != '' ): ?>
							<a class="navbar-brand" href="http://easycomingsoon.com">
								<img src="<?php echo $current_options['logo']; ?>" class="logo" alt="<?php bloginfo('name'); ?>">
							</a>
							<?php endif; ?>
							
							<!-- ============================================================= LOGO : END ============================================================= -->
							
							
							<!-- ============================================================= MAIN NAVIGATION ============================================================= -->
							
							<?php 
							
							$extra_html = '<ul id="%1$s" class="%2$s">%3$s';
							$extra_html .= '<li>
									<div class="signup">
										<a href="'.$current_options['signup_url'].'" class="btn-default btn-mandy">'.($current_options['signup_text']!=''?$current_options['signup_text']:'Sign up').'</a>
										<a  href="'.$current_options['login_url'].'" class="btn-default btn-dark">'.($current_options['login_text']!=''?$current_options['login_text']:'Login').'</a>
									</div>
								</li>';
							$extra_html .= '</ul>';
							
							wp_nav_menu( array(
							'theme_location' => 'primary',
							'container'  => 'nav-collapse collapse navbar-inverse-collapse',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => 'webriti_fallback_page_menu',
							'items_wrap'  => $extra_html,
							'walker' => new webriti_nav_walker()) 
							); ?>
							
							<!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->
							
						</div><!-- /.container -->
					</div><!-- /.navbar-collapse -->
				</div><!-- /.yamm -->
			</div><!-- /.navbar -->
		</header>
		
		<!-- ============================================================= HEADER : END ============================================================= -->