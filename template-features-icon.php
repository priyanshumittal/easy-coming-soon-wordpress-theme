<?php 
/*
 * Template Name: Features Page with Icons
 * @package WordPress
 * @subpackage esc
 * @since esc 0.1
 */
get_header(); ?>
<section id="services">
				<div class="container inner-top inner-bottom-sm">
					
					<!-- /.row -->
					
									
					<div class="row">
						<div class="col-md-8 col-sm-9 center-block text-center">
							<header>
								<h1>Plugin Features</h1>
								<!--<p>Magnis modipsae que voloratati andigen daepeditem quiate re porem aut labor. Laceaque quiae sitiorem rest non restibusaes maio es dem tumquam.</p>-->
							</header>
						</div><!-- /.col -->
					</div><!-- /.row -->
					
					<div class="row inner-top-sm text-center">
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-edit"></i>
							</div><!-- /.icon -->
							<h3>Realtime Page Customizer</h3>
							<p class="text-small">Just like WordPress Theme Customizer plugin has in-built realtime page Customizer, helps you in creating coming soon pages in a flash.</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-list"></i>
							</div><!-- /.icon -->
							<h3>Multiple Design Templates</h3>
							<p class="text-small">Plugin comes with 6 pre-defined templates which act as a great starting point for your landing page. <a href="http://easycomingsoon.com/themes/">View All Premium Templates</a></p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-mobile"></i>
							</div><!-- /.icon -->
							<h3>Mobile Friendly</h3>
							<p class="text-small">All templates are Responsive out of the box. Landing pages display perfectly on mobile and tablet devices.</p>
						</div><!-- /.col -->
						
						
					</div><!-- /.row -->
					
					<div class="row text-center">
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-database"></i>
							</div><!-- /.icon -->
							<h3>Subscriber DataBase</h3>
							<p class="text-small">Quickly capture and store visitor email address in wordpress database. You can also export the email address</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-language"></i>
							</div><!-- /.icon -->
							<h3>Translation Ready</h3>
							<p class="text-small">Quickly Translate plugin to the language of your choice</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-envelope-o"></i>
							</div><!-- /.icon -->
							<h3>Newsletter Integration</h3>
							<p class="text-small">Quickly Integrate with Autoresponser Services like Mailchimp, Campaign Monitor, Aweber etc.</p>
						</div><!-- /.col -->
						
					</div><!-- /.row -->
					
					
					
					<div class="row text-center">
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-comments"></i>
							</div><!-- /.icon -->
							<h3>Custom Thank you Message</h3>
							<p class="text-small">Send a custom thank you note to your subscribers. The message is editable from the dashboard</p>
						</div><!-- /.col -->
						
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-rocket"></i>
							</div><!-- /.icon -->
							<h3>Powerful Design Settings.</h3>
							<p class="text-small">Quickly add custom logo, custom font, custom background etc from the dashboard. No need to dabble with the code.</p>
						</div><!-- /.col -->
						
							<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-image"></i>
							</div><!-- /.icon -->
							<h3>Multiple Background Types</h3>
							<p class="text-small">You can add Static Image Background, Image Slider BackGround and Video Background.</p>
						</div><!-- /.col -->
					</div>	
					
					<div class="row text-center">
						<div class="col-sm-4 inner-bottom-xs">
							<div class="icon">
								<i class="fa fa-lock"></i>
							</div><!-- /.icon -->
							<h3>Access Control</h3>
							<p class="text-small">Configure which users can view the live site. White List IP and Users. This feature is perfect to share the website privately with the client.</p>
						</div><!-- /.col -->
					</div>
					
<!-- /.row -->
					
					
					
					
									
				</div><!-- /.container -->
			</section>
			
			
	<?php get_template_part('template','callout'); ?>
	
<?php get_footer(); ?>