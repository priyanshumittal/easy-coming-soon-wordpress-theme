<?php 
class Webriti_pagination
{
	function Webriti_page()
	{
			global $post;
			global $wp_query, $wp_rewrite,$loop;
			$paged  = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
			if($wp_query->max_num_pages==0)
			{
				$wp_query = $loop;
			}
	?>
	<ul class="pagination">
		<?php if( $paged != 1  ) : ?>
		<li><a href="<?php echo get_pagenum_link(($paged-1 > 0 ? $paged-1 : 1)); ?>"><?php _e('Prev','ecs'); ?></a></li>
		<?php endif; ?>
		
		<?php for($i=1;$i<=($wp_query->max_num_pages>1 ? $wp_query->max_num_pages : 0 );$i++){ ?>
		<li class="<?php echo ($i == $paged ? 'active ' : ''); ?>"><a href="<?php echo get_pagenum_link($i); ?>"><?php echo $i; ?></a></li>
		<?php } ?>
		
		<?php if($wp_query->max_num_pages!= $paged): ?>
		<li><a href="<?php echo get_pagenum_link(($paged+1 <= $wp_query->max_num_pages ? $paged+1 : $wp_query->max_num_pages )); ?>"><?php _e('Next','ecs'); ?></a></li>
		<?php endif; ?>
	</ul><!-- /.pagination -->
<?php 				
  } 
}
?>