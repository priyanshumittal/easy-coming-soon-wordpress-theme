<?php	
add_action( 'widgets_init', 'ecs_widgets_init');
function ecs_widgets_init() {

	/*sidebar*/
	register_sidebar( array(
		'name' => __( 'Sidebar Widget Area', 'ecs' ),
		'id' => 'sidebar_primary',
		'description' => __( 'The right sidebar widget area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
		
	register_sidebar( array(
		'name' => __( 'Footer 1', 'ecs' ),
		'id' => 'footer1',
		'description' => __( 'footer 1 sidebar area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer 2', 'ecs' ),
		'id' => 'footer2',
		'description' => __( 'footer 2 sidebar area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer 3', 'ecs' ),
		'id' => 'footer3',
		'description' => __( 'footer 3 sidebar area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer 4', 'ecs' ),
		'id' => 'footer4',
		'description' => __( 'footer 4 sidebar area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Contact Template Sidebar', 'ecs' ),
		'id' => 'contact',
		'description' => __( 'contact page sidebar area', 'ecs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
}