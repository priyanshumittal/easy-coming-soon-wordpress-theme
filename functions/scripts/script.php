<?php

// Webriti scripts
if( !function_exists('ecs_scripts_function') )
{
	function ecs_scripts_function(){
		
		// css files
		wp_enqueue_style('ecs-bootstrap', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.min.css' );
		
		wp_enqueue_style('ecs-lato', '//fonts.googleapis.com/css?family=Lato:400,900,300,700' );
		wp_enqueue_style('ecs-Source-sans', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic' );
		wp_enqueue_style('ecs-fontello', WEBRITI_TEMPLATE_DIR_URI . '/fonts/fontello.css' );
		wp_enqueue_style('ecs-font-awesome', WEBRITI_TEMPLATE_DIR_URI . '/fonts/font-awesome/css/font-awesome.css' );
		
		if(is_front_page()){
		wp_enqueue_style('ecs-owl-theme', WEBRITI_TEMPLATE_DIR_URI . '/css/owl.carousel.css' );
		wp_enqueue_style('ecs-owl-transitions', WEBRITI_TEMPLATE_DIR_URI . '/css/owl.transitions.css' );
		}
		
		wp_enqueue_style('ecs-style', get_stylesheet_uri() );
		wp_enqueue_style('ecs-theme-color', WEBRITI_TEMPLATE_DIR_URI . '/css/mandy.css' );
		// js files
		wp_enqueue_script( 'jquery' );
		
	}
}
add_action('wp_enqueue_scripts','ecs_scripts_function');

// webriti footer enquque scripts
if( !function_exists('ecs_footer_enquque_scripts') )
{
	function ecs_footer_enquque_scripts()
	{
		// js file
		wp_enqueue_script( 'jquery-bootstrap' , WEBRITI_TEMPLATE_DIR_URI . '/js/bootstrap.min.js' );
		wp_enqueue_script( 'jquery-hover-dropdown' , WEBRITI_TEMPLATE_DIR_URI . '/js/bootstrap-hover-dropdown.min.js' );
		wp_enqueue_script( 'jquery-waypoints' , WEBRITI_TEMPLATE_DIR_URI . '/js/waypoints.min.js' );
		wp_enqueue_script( 'jquery-waypoints-sticky' , WEBRITI_TEMPLATE_DIR_URI . '/js/waypoints-sticky.min.js' );
		
		if(is_front_page()){
			wp_enqueue_script( 'jquery-owl-carousel' , WEBRITI_TEMPLATE_DIR_URI . '/js/owl.carousel.min.js' );
			wp_enqueue_script( 'jquery-easytabs' , WEBRITI_TEMPLATE_DIR_URI . '/js/jquery.easytabs.min.js' );
			wp_enqueue_script( 'owl-custom' , WEBRITI_TEMPLATE_DIR_URI . '/js/owl.custom.js' );
			wp_enqueue_script( 'easytabs-custom' , WEBRITI_TEMPLATE_DIR_URI . '/js/easytabs.custom.js' );
		}
		
		wp_enqueue_script( 'jquery-scripts' , WEBRITI_TEMPLATE_DIR_URI . '/js/scripts.js' );
	}
}
add_action('wp_footer','ecs_footer_enquque_scripts');