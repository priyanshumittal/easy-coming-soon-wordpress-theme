<?php 
function ecs_contact_page_customizer( $wp_customize ){


	$wp_customize->add_section( 'contact_settings' , array(
		'title'      => __('Contact Template Settings', 'ecs'),
		'priority'   => 126,
   	) );
	
	$wp_customize->add_setting( 'ecs_option[contact_title]', array(
	'default' => __('Contact Us','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[contact_title]', array(
	'label'     => __( 'Title', 'ecs' ),
	'section'   => 'contact_settings',
	) );
	
	$wp_customize->add_setting( 'ecs_option[contact_desc]', array(
	'default' => __('Have questions? We’re happy to help!','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[contact_desc]', array(
	'label'     => __( 'Description', 'ecs' ),
	'section'   => 'contact_settings',
	'type'=>'textarea',
	) );
}
add_action( 'customize_register', 'ecs_contact_page_customizer' );