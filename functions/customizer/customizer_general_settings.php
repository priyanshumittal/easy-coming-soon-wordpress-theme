<?php 
function ecs_general_settings_customizer( $wp_customize ){

	/* Quicl Start settings */
	$wp_customize->add_section( 'logo_settings' , array(
		'title'      => __('Logo', 'ecs'),
		'priority'   => 125,
   	) );
	
	//Header logo
	$wp_customize->add_setting( 'ecs_option[logo]', array(
	
	'default' => get_template_directory_uri().'/images/logo.png',
    'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'esc_url_raw',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize,'ecs_option[logo]', array(
	'label'     => __( 'Upload a 267 x 51 for Logo Image', 'ecs' ),
	'section'   => 'logo_settings',
	) )	);	
}
add_action( 'customize_register', 'ecs_general_settings_customizer' );