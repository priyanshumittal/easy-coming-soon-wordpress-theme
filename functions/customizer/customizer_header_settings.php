<?php 
function ecs_header_customizer( $wp_customize ){


	$wp_customize->add_section( 'header_settings' , array(
		'title'      => __('Header Settings', 'ecs'),
		'priority'   => 128,
   	) );
	
	$wp_customize->add_setting( 'ecs_option[signup_url]', array(
	'default' => __('#','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[signup_url]', array(
	'label'     => __( 'Signup URL', 'ecs' ),
	'section'   => 'header_settings',
	'type'      => 'text',
	) );
	
	$wp_customize->add_setting( 'ecs_option[signup_text]', array(
	'default' => __('Sign up','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[signup_text]', array(
	'label'     => __( 'Signup Text', 'ecs' ),
	'section'   => 'header_settings',
	'type'=>'text',
	) );
	
	
	$wp_customize->add_setting( 'ecs_option[login_url]', array(
	'default' => __('#','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[login_url]', array(
	'label'     => __( 'Login URL', 'ecs' ),
	'section'   => 'header_settings',
	'type'      => 'text',
	) );
	
	$wp_customize->add_setting( 'ecs_option[login_text]', array(
	'default' => __('Login','ecs'),
    'capability'     => 'edit_theme_options',
	'type' => 'option'
    ));
	
	$wp_customize->add_control( 'ecs_option[login_text]', array(
	'label'     => __( 'Login Text', 'ecs' ),
	'section'   => 'header_settings',
	'type'=>'text',
	) );
}
add_action( 'customize_register', 'ecs_header_customizer' );