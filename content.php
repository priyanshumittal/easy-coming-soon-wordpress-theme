<?php
/**
 * The template part for displaying content
 *
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

		<?php ecs_post_thumbnail(); ?>
		
		<div class="post-content">	
		
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			</header>
			
			<?php if( !is_page() ) : ?>
			<div class="entry-meta">
				<span class="entry-date"><a href="<?php echo get_month_link(get_post_time('Y'),get_post_time('m')); ?>"><time datetime=""><?php echo get_the_date('M j, Y'); ?></time></a></span>
				<span class="comments-link"><a href="<?php the_permalink(); ?>"><?php comments_popup_link( __( 'Leave a comment', 'ecs' ) ); ?></a></span>
				<?php the_tags( '<span class="tag-links">', ', ', '</span>' ); ?>					
			</div>
			<?php endif; ?>
			
			<div class="entry-content">
				<?php 
					the_content( sprintf( __( 'Read More', 'ecs' ) ) );
				?>
			</div>
			
		</div>
</article>