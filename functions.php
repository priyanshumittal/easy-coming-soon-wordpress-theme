<?php

// Global variables define
define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
define('WEBRITI_TEMPLATE_DIR',get_template_directory());
define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');

require( WEBRITI_TEMPLATE_DIR . '/theme_setup_data.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/script.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/sidebars.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_general_settings.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_contact_page.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_header_settings.php');


// Webriti theme title
if( !function_exists( 'ecs_head_title' ) ) 
{
	function ecs_head_title( $title , $sep ) {
		global $paged, $page;
		
		if ( is_feed() )
				return $title;
			
		// Add the site name
		$title .= get_bloginfo( 'name' );
		
		// Add the site description for the home / front page
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
				$title = "$title $sep $site_description";
			
		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
				$title = "$title $sep " . sprintf( __( 'Page %s', 'hotel' ), max( $paged, $page ) );
			
		return $title;
	}
}
add_filter( 'wp_title', 'ecs_head_title', 10, 2);


// Theme setup
if( !function_exists( 'ecs_theme_setup' ) )
{
	function ecs_theme_setup(){
		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 600;
		
		// Load text domain for translation-ready
		load_theme_textdomain( 'ecs', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );
		
		// supports featured image
		add_theme_support( 'post-thumbnails' );
		
		// main menu navigation bar
		register_nav_menu( 'primary', __( 'Primary Menu', 'ecs' ) );
		register_nav_menu( 'footer', __( 'Footer Menu', 'ecs' ) );
		
		// shortcode support
		add_filter('widget_text', 'do_shortcode');
	}
}
add_action( 'after_setup_theme', 'ecs_theme_setup' );


if ( ! function_exists( 'ecs_post_thumbnail' ) ) :
/**
 * Displays an optional post thumbnail.
 *
 */
function ecs_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<figure class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</figure>

	<?php else : ?>
	
	<figure class="post-thumbnail">
			<a href="<?php the_permalink(); ?>"><span class="icn-more"></span><?php the_post_thumbnail(); ?></a>
	</figure>

	<?php endif; // End is_singular()
}
endif;


function ecs_comment($comment, $args, $depth) {
    
       $GLOBALS['comment'] = $comment; ?>
       
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
			<article class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<?php echo get_avatar($comment,$size = '60'); ?>						
						<b class="fn"><a href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>" class="url"><?php the_author();?></a></b> 
						<span class="says"><?php _e("says:","ecs"); ?></span>					
					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>"><time datetime="<?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?>"><?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?></time></a>
					</div><!-- .comment-metadata -->
				</footer><!-- .comment-meta -->
				
				<?php if ($comment->comment_approved == '0') : ?>
					<em><?php _e('Your comment is awaiting moderation.') ?></em><br />
				<?php endif; ?>
					
				<div class="comment-content">
					<?php comment_text(); ?>
				</div><!-- .comment-content -->

				<div class="reply">
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			
			</article><!-- .comment-body -->
		</li>
<?php } 

function ecs_user_info_fields_remove() {
	remove_action( 'edd_purchase_form_after_user_info', 'edd_user_info_fields' );
	remove_action( 'edd_register_fields_before', 'edd_user_info_fields' );
}
add_action( 'init', 'ecs_user_info_fields_remove' );

function ecs_user_info_fields() {

	$customer = EDD()->session->get( 'customer' );
	$customer = wp_parse_args( $customer, array( 'first_name' => '', 'last_name' => '', 'email' => '' ) );

	if( is_user_logged_in() ) {
		$user_data = get_userdata( get_current_user_id() );
		foreach( $customer as $key => $field ) {

			if ( 'email' == $key && empty( $field ) ) {
				$customer[ $key ] = $user_data->user_email;
			} elseif ( empty( $field ) ) {
				$customer[ $key ] = $user_data->$key;
			}

		}
	}

	$customer = array_map( 'sanitize_text_field', $customer );
	?>
	<fieldset id="edd_checkout_user_info">
		<span><legend><?php echo apply_filters( 'edd_checkout_personal_info_text', __( 'Personal Info', 'easy-digital-downloads' ) ); ?></legend></span>
		
		<div class="row">
			<div class="col-md-6">
					<p id="edd-first-name-wrap">
					<label class="edd-label" for="edd-first">
						<?php _e( 'First Name', 'easy-digital-downloads' ); ?>
						<?php if( edd_field_is_required( 'edd_first' ) ) { ?>
							<span class="edd-required-indicator">*</span>
						<?php } ?>
					</label>
					<span class="edd-description"><?php _e( 'We will use this to personalize your account experience.', 'easy-digital-downloads' ); ?></span>
					<input class="edd-input required" type="text" name="edd_first" placeholder="<?php _e( 'First name', 'easy-digital-downloads' ); ?>" id="edd-first" value="<?php echo esc_attr( $customer['first_name'] ); ?>"<?php if( edd_field_is_required( 'edd_first' ) ) {  echo ' required '; } ?>/>
				</p>
			</div>
			
			<div class="col-md-6">
				<p id="edd-last-name-wrap">
					<label class="edd-label" for="edd-last">
						<?php _e( 'Last Name', 'easy-digital-downloads' ); ?>
						<?php if( edd_field_is_required( 'edd_last' ) ) { ?>
							<span class="edd-required-indicator">*</span>
						<?php } ?>
					</label>
					<span class="edd-description"><?php _e( 'We will use this as well to personalize your account experience.', 'easy-digital-downloads' ); ?></span>
					<input class="edd-input<?php if( edd_field_is_required( 'edd_last' ) ) { echo ' required'; } ?>" type="text" name="edd_last" id="edd-last" placeholder="<?php _e( 'Last name', 'easy-digital-downloads' ); ?>" value="<?php echo esc_attr( $customer['last_name'] ); ?>"<?php if( edd_field_is_required( 'edd_last' ) ) {  echo ' required '; } ?>/>
				</p>
			</div>
		</div>
		
		<?php do_action( 'edd_purchase_form_before_email' ); ?>
		<p id="edd-email-wrap">
			<label class="edd-label" for="edd-email">
				<?php _e( 'Email Address', 'easy-digital-downloads' ); ?>
				<?php if( edd_field_is_required( 'edd_email' ) ) { ?>
					<span class="edd-required-indicator">*</span>
				<?php } ?>
			</label>
			<span class="edd-description"><?php _e( 'a confirmation email will be sent to you at this address', 'easy-digital-downloads' ); ?></span>
			<input class="edd-input required" type="email" name="edd_email" placeholder="<?php _e( 'Email address', 'easy-digital-downloads' ); ?>" id="edd-email" value="<?php echo esc_attr( $customer['email'] ); ?>"/>
		</p>
		<?php do_action( 'edd_purchase_form_after_email' ); ?>
		
		<?php do_action( 'edd_purchase_form_user_info' ); ?>
		<?php do_action( 'edd_purchase_form_user_info_fields' ); ?>
	</fieldset>
	<?php
}
add_action( 'edd_purchase_form_after_user_info', 'ecs_user_info_fields' );
add_action( 'edd_register_fields_before', 'ecs_user_info_fields' );

function ecs_edd_payment_gateways( $gateways ) {
	// change this to match the name of your payment gateway
	$gateways['2checkout'] = array( 
		'admin_label' 		=> '2Checkout',  // this is the label shown in downloads -> settings -> payment gateways
		'checkout_label' 	=> 'Credit Card ( Pay via Credit Card with 2Checkout secure card processing. )' // this label is shown at checkout. Here we've changed it from "2Checkout" to "Credit Card"
	);
	
	return $gateways;
}
add_filter( 'edd_payment_gateways', 'ecs_edd_payment_gateways' );